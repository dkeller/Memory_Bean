'use strict';
// The file contain the game(Partie) Constructor
function Game(namePlayer, difficulty, theme){
    // Game Information
    this.rows = 3;
    this.columns = 4 + (difficulty-1) * 2;

    // Player information
    this.playerName = namePlayer;
    this.difficulty = difficulty;
    this.theme = theme;
    this.nbPairFound = 0;
    this.nbMove=0;

    // Section Chrono
    this.duration = 3 * 60 * 1000 - (difficulty-1) * 30 * 1000 ; // delta of time in milliseconds
    const DELTA = 1000;
    var lastStart; // the time start of the initialisation of the game
    var sumPassTime;
    var sumTimeLeft= this.duration;
    var now;
    var isFirstTurn = true;

    // Methods
    this.startChrono = function(){
        // console.log('start');
        lastStart = new Date().getTime();
        sumPassTime = lastStart;
    };
    this.reStartChrono = function(){
        // console.log('reStart');
        lastStart = new Date().getTime();
        isFirstTurn=false;
    };
    this.getPassTime = function(){
        if(isFirstTurn){
            now = new Date().getTime();
            sumPassTime = (now - lastStart);
        } else {
            sumPassTime = sumPassTime + 1000;
        }
        // console.log('sumPassTime : ', utcToHHMMSS(sumPassTime));
        return sumPassTime; // delta of time in milliseconds
    };
    this.displayPassTime = function() {
        if(isFirstTurn){
            now = new Date().getTime();
            sumPassTime = (now - lastStart);
        } else {
            sumPassTime = sumPassTime + 1000;
        }
        // console.log('sumPassTime : ', utcToHHMMSS(sumPassTime));
        return utcToHHMMSS(sumPassTime); // delta of time in milliseconds
    };

    this.displayTimeLeft = function() {
        sumTimeLeft = (this.duration - sumPassTime);
        // console.log('sumTimeLeft : ', utcToHHMMSS(sumTimeLeft));
        return utcToHHMMSS(sumTimeLeft); // delta of time in milliseconds
    };

    //Payer Section
    this.setPalyerName = function(playerName) {
        this.playerName = playerName;
    };
    this.addPairFound = function(){
        return this.nbPairFound++;
    };
    this.displayNbPairFound = function(){
        return this.nbPairFound;
    };
    this.addMove = function(){
        return this.nbMove++;
    };
    this.displayNbMove = function(){
        return this.nbMove;
    };
    this.getPlayerObject = function(){
    var playerObject = {
        PlayerName : this.playerName,
        Time : this.getPassTime(),
        NbMove : this.nbMove
    };
    // console.log('playerObject : ',playerObject);
    return JSON.stringify(playerObject);
    }
}