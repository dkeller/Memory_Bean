"use strict";
//Constants
const ONE_SECOND = 1000;

// variables
var timerId;
var currentPlay;
var isFirstLauch = true;
var isChronoOn=false;

timerId = setInterval(function(){
    // console.log('test of the setInterval');
    if(isChronoOn){
        $('#TempsImparti + p').html( utcToHHMMSS(currentPlay.duration));
        $('#tempsEcole + p').html( currentPlay.displayPassTime() );
        $('#tempsRestant + p').html( currentPlay.displayTimeLeft() );
        $('#nombreEssai + p').html(currentPlay.displayNbMove() );
        $('#nombrePairRevele + p').html(currentPlay.displayNbPairFound() );
    }
},ONE_SECOND);

function resetdisplay(){
    $('#TempsImparti + p').html( '00:00' );
    $('#tempsEcole + p').html( '00:00' );
    $('#tempsRestant + p').html( '00:00' );
    $('#nombreEssai + p').html( '0' );
    $('#nombrePairRevele + p').html( '0' );
}

// This function return UTC to HH:MM:SS
function utcToHHMMSS(utcTime){
    // console.log('utcTime : ', utcTime);
    var utcTime = utcTime/1000; // delta in seconds
    var minutes= Math.floor(utcTime/60);
    var seconds = Math.floor(utcTime - minutes*60);
    if(seconds < 10){
        seconds = '0' + seconds;
    }
    if( minutes < 10){
        minutes = '0' + minutes;
    }
    return minutes +':'+seconds;
}

function addToScoreboard(arrayPlayer){
    $("#list_score").empty();
    for(var i=0; i < arrayPlayer.length; i++){
        var tempString = (arrayPlayer[i]);

         $('#list_score').append('<li>'+tempString.PlayerName +'  -  '+ utcToHHMMSS(tempString.Time)+'  -  '+tempString.NbMove+'</li>');
    }
}

function addToLocalStorage(arrayToStore){
    var key = 'key';

    var arrayToStore = JSON.stringify(arrayToStore);
    // console.log('arrayToStore : ', arrayToStore);
    localStorage.setItem( key, arrayToStore );
};

function getLocalStorage(arrayKey){
    return JSON.parse(localStorage.getItem( arrayKey ));
};

function addToArray(arrayDest, toAdd){
    if(toAdd !== null){
        // console.log('toAdd.length  : ', toAdd.length);
        for(var i=0; i<toAdd.length; i++){
            arrayDest.push(toAdd[i]);
        }
    }
    return arrayDest;
}

// Ex : arrayTest.sort(sortByAttribute("Time"));
function sortByAttribute(attr) {
    return function(a, b) {
        if (a[attr] > b[attr]) {
            return 1;
        } else if (a[attr] < b[attr]) {
            return -1;
        }
        return 0;
    }
}