"use strict";
const NUMBER_ROWS = 4;
const NUMBER_COLUMNS = 7;
const TIME_PRESENT_CARDS = 5000;
var cards = [];
var gamebox = document.getElementById("gamebox");
var counter = 0;

// Constants
const DURATION = 3*60*1000; // in milliseconds

// Variables
var currentPlay;
var playerInfo;
var arrayPlayer = [
    // {PlayerName: "Sebastien Blais", Time: 19573, NbMove: 7},
    // {PlayerName: "Dimitri Keller", Time: 32573, NbMove: 12},
    // {PlayerName: "Sebastien Blais", Time: 20573, NbMove: 8}
];

var Card = function (cardNumber) {
    this.id = counter++;
    this.cardNumber = cardNumber;
    this.faceUp = true;
    this.img = "images/card" + cardNumber + ".jpg";
    this.cover = "images/cover1.png";
};
Card.prototype.flipCard = function () {
    $("#" + this.id).toggleClass("flipped"); // flip DIV

    var card = get_card(this.id);
    card.faceUp = !card.faceUp;
};

function create_cards(numberOfCards) {
    for (var i = 0; i < numberOfCards / 2; i++) { // add pair of cards with same cardNumber i
        cards.push(new Card(i));
        cards.push(new Card(i));
    }
    return cards;
}

function draw_card(card) {
    var divContainer = document.createElement("div");
    divContainer.className = "container";
    var div = document.createElement("div");
    div.id = card.id;
    div.className = "card";

    divContainer.style.width = 100 / currentPlay.columns + '%';
    divContainer.style.height = 150 + "px";

    var divFront = document.createElement("div");
    var divBack = document.createElement("div");
    divFront.className = "front";
    divBack.className = "back";

    var imgFront = document.createElement("img");
    imgFront.src = card.img;

    var imgBack = document.createElement("img");
    imgBack.src = card.cover;

    divFront.appendChild(imgFront);
    divBack.appendChild(imgBack);

    div.appendChild(divFront);
    div.appendChild(divBack);

    divContainer.appendChild(div);
    gamebox.appendChild(divContainer);
}
function draw_cards(cards) {
    for (var indice in cards) {
        draw_card(cards[indice]);
    }
}
function get_card(id) {
    var i = 0;
    while (cards[i].id !== parseInt(id)) {
        i++;
    }
    return cards[i];
}
function flip_cards(cards) {
    for (var indice in cards) {
        cards[indice].flipCard();
    }
}
function add_onclick_divCards() {
    var divCards = document.getElementsByClassName("card");
    var firstCardSelection = true, clickEnabled = true;
    var firstCardSelectedCardNumber,firstCardId, secondCardId ;
    var maxNumberOfPairs= currentPlay.rows*currentPlay.columns/2;
    var numberOfPairs = 0;

    for (var i = 0; i < divCards.length; i++) {
        divCards[i].addEventListener("click", flipCardFunction);
    }

    function flipCardFunction() {
        // First Card Selected
        if (firstCardSelection && clickEnabled) {
            firstCardSelection = false;
            firstCardId = this.id;
            firstCardSelectedCardNumber = get_card(firstCardId).cardNumber;

            get_card(firstCardId).flipCard();
            this.removeEventListener("click", flipCardFunction);
        }
        // Second Card Selected
        else if (clickEnabled) {
            firstCardSelection = true;
            secondCardId = this.id;
            get_card(secondCardId).flipCard();

            // Second Card corresponds to First Card
            if (get_card(secondCardId).cardNumber === firstCardSelectedCardNumber) {
                this.removeEventListener("click", flipCardFunction);
                numberOfPairs++;
                currentPlay.addPairFound();
                // Test Victory
                if(numberOfPairs === maxNumberOfPairs){

                    setTimeout(function () {

                        // Initialisation de l'array arrayPlayer
                        arrayPlayer = [];
                        // Get information from localStorage
                        var localStorageObject = getLocalStorage('key');
                        arrayPlayer = addToArray(arrayPlayer, localStorageObject);

                        var playerInfoString = currentPlay.getPlayerObject();
                        playerInfo = JSON.parse(playerInfoString);
                        arrayPlayer.push(playerInfo);
                        arrayPlayer.sort(sortByAttribute("Time"));

                        addToLocalStorage(arrayPlayer);
                        var scoreboard = getLocalStorage('key');
                        addToScoreboard(scoreboard);

                        clearInterval(timerId);
                        // Display win message
                        window.alert("Vous avez gagné !");
                        // location.reload();
                    }, 1000);
                }
            }
            // Second Card does not correspond to First Card
            else {
                // console.log('rate');
                clickEnabled = false;

                setTimeout(function () {
                    // Flip back the two cards to their original position and add Listeners back
                    get_card(secondCardId).flipCard();
                    document.getElementById(secondCardId).addEventListener("click", flipCardFunction);

                    get_card(firstCardId).flipCard();

                    document.getElementById(firstCardId).addEventListener("click", flipCardFunction);
                    clickEnabled = true;
                }, 2000);
            }
            // Test to display the number of Move
            currentPlay.addMove();
        }
    }

    function remove_eventlisteners(){
        for (var i = 0; i < divCards.length; i++) {
            divCards[i].removeEventListener("click", flipCardFunction);
        }
    }
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

function button_management(){
    // START LIGHTBOX
    $("#btn_newGame").on("click", function() {
        // Ouverture de la fenêtre de saisie de l'information du joueur

        mb_open('#new_game_lightbox',null);
    });

    $('#play_new_game_modal_box').on('click', function() {
        var lightbox = ($(this).parent().parent());
        var namePlayer = lightbox.find('#namePlayer').val();
        var difficulty = lightbox.find('#difficulty').val();
        var theme =  lightbox.find('#theme').val();
        // console.log(namePlayer, difficulty, theme);

        mb_close();
        $('#userName h4:nth-child(2)').text(namePlayer);

        delete_game(); // init
        create_game(namePlayer, difficulty, theme);

    }); // END LIGHTBOX

    $('#difficulty').on('change', function () {
        console.log($(this).val());
        var lightbox = ($(this).parent().parent());

        if (parseInt($(this).val()) === 1) {
            console.log('change');
            lightbox.find('#rows').text(3);
            lightbox.find('#columns').text(4);
            lightbox.find('#temps_limite').text(3 + " min");
        }
        else if (parseInt($(this).val()) === 2) {
            lightbox.find('#rows').text(3);
            lightbox.find('#columns').text(7);
            lightbox.find('#temps_limite').text(2 + ":" + 30 + " min");
        }
        else if (parseInt($(this).val()) === 3) {
            lightbox.find('#rows').text(3);
            lightbox.find('#columns').text(8);
            lightbox.find('#temps_limite').text(2 + " min");
        }
    });

    $('#btn_quitGame').on('click', function(){
        if (confirm('Voulez vous terminer la partie ? ') === true) {
            isChronoOn = false;
        }
    });

    $('#btn_play_pause').on('click', function(){
        if(isFirstLauch){
            currentPlay.reStartChrono();
            $(this).text('Play');
            isChronoOn = false;
            isFirstLauch = false;
            add_onclick_divCards().remove_eventlisteners();
        } else {
            if(isChronoOn){
                $(this).text('Play');
                isChronoOn = false;
                add_onclick_divCards().remove_eventlisteners();
            } else {
                currentPlay.reStartChrono();
                $(this).text('Pause');
                isChronoOn = true;
                add_onclick_divCards();
            }
        }
    });

    $('#btn_stop').on('click', function(){
        if (confirm('Voulez vous terminer la partie ? ') === true) {
            isChronoOn = false;
        }
    });

    $("#btn_theme").on("click", function() {
        var theme_css = $("#theme_css");
        if(theme_css.attr("href") === 'style/theme0.css'  )
        theme_css.attr("href", 'style/theme1.css');
        else{
            theme_css.attr("href", 'style/theme0.css');
        }
    });
}

function create_game(namePlayer, difficulty, theme){
    currentPlay = new Game(namePlayer, difficulty, theme);
    // console.log(theme);
    if(theme === 'Espace'){
        $("#theme_css").attr("href", 'style/theme1.css');
    }
    cards = create_cards(currentPlay.rows * currentPlay.columns);
    cards = shuffle(cards);
    draw_cards(cards);
    resetdisplay();

    // Delta time to give a moment to the player to see the cards
    setTimeout(function(){
        flip_cards(cards);
        isChronoOn = true;
        currentPlay.startChrono();
        add_onclick_divCards();
    }, TIME_PRESENT_CARDS);
}

function delete_game(){
    // initialization Gamebox
    $("#gamebox").children().remove();
    cards = [];
}
// main function of the script
$(function(){
    console.log('DOM chargé');
    button_management();
});