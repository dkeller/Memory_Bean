"use strict";
/**
 * Created by dimitrikeller on 19/05/2017.
 */
/**
 * La boite modal est creee en appelant une fonction mb_open()
 * -- placee en fin de body
 * -- fermee en appelant mb_close()
 * -- Elle gere les evenements de fermeture au click
 */
var mb_data = {
    container: null // Le container de la boite modale
};

/**
 * Rechercher le container et s'il n'existe pas, le cree
 */
function mb_create_container() {
    mb_data.container = $('.mb_container');
    if (mb_data.container.length === 0) { // N'existe pas : le creer
        mb_data.container = $('<div>')
            .addClass('mb_container')
            .appendTo('body');
        // Creation du rideau qui repond au click
        $('<div>')
            .appendTo(mb_data.container)
            .addClass('mb_background')
            .on('click', function () {
                mb_close();
            });
        console.log('mb_container cree');


    }
}
/**
 * Afficher la modal box et son contenu
 * @param target: Selecteur de l'element a afficher
 * @param closingitems : Selecteur du et des elements qui doivent fermer la mb
 * @param css_options : Options css a surcharger dans le fond du container
 */
function mb_open(target, closingitems, css_options) {
    console.log('ouverture de ', target);
    if (mb_data.container === null) {
        mb_create_container();
    }
    // Ici la modal box est creee
    if ('undefined' !== typeof css_options) {
        mb_data.container.find('.mb_background').css(css_options);
    }

    // Le contenu: cloner la target
    let clone = $(target)
        //.clone(true, true)
        .addClass('mb_item')
        .appendTo(mb_data.container)
        .show()
        .parent()
        .fadeIn();
    if('undefined' !== typeof closingitems){
        clone
            .find(closingitems)
            .on('click', function () {
                mb_close();
            })
    }

}

/**
 * Fermer la mb
 */
function mb_close(){
    console.log('Fermeture de la modal box');
    mb_data.container
        .fadeOut(1000, function () {
            $(this)
                .find('.mb_item')
        });
}
//addEvent Listeners
// $('#btn_play_new_game').on('click', function(){
//     console.log('btn_play_new_game');
//     testChrono = new Game(DURATION);
//     testChrono.startChrono();
//     isChronoOn = true;
// });
// $('#btn_cancel').on('click', function(){
//     console.log('cancel');
// });
